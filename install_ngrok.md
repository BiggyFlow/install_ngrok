#### Ngrok Linux Package

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

#### 1. run this command :
```sh
wget https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-linux-amd64.tgz
tar zxvf ngrok-v3-stable-linux-amd64.tgz 
mv ngrok /usr/local/bin
```
#### 2. create an account on the ngrok website
--> [ngrok website](https://dashboard.ngrok.com/signup)

#### 3. adding the token recovered from your account on the ngrok site
```sh
ngrok config add-authtoken "xxxxxxxxxx your token xxxxxxxxxxxxxx"
```
#### 4. Run ngrok related to the local listening port of your application. Example : 
```sh
ngrok http 80
```
### Enjoy !
